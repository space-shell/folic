/*
____________/\\\\\\\\\\\\_______________
____________\/\\\////////\\\____________
_____________\/\\\______\//\\\__________
______________\/\\\_______\/\\\_________
__________/\\\_\/\\\_______\/\\\________
__________\///__\/\\\_______\/\\\_______
_________________\/\\\_______/\\\_______
_____________/\\\_\/\\\\\\\\\\\\/_______
_____________\///__\////////////________
*/

import Zya from "https://space-shell.gitlab.io/zya/zya.js";
import PointerEvents from "https://space-shell.gitlab.io/zya/processes/zya-pointer-events.js";

import * as folioContent from "./partials/folio-content.js";

import { LoopWrite } from "./components/loop-write.js";

customElements.define("loop-write", LoopWrite);

Zya("main.content", {
  document({ state }) {
    if (state === "ready") {
      this.style.setProperty(
        "--bg-pos",
        `${new Date().getHours() * (100 / 24)}%`,
      );
    }
  },
});

Zya("#folio-content", {
  get state() {
    return {
      partials: folioContent,
    };
  },

  projectClosed() {
    this.classList.remove("active");
  },

  projectSelect({ projectName }) {
    if (projectName === undefined) return;

    if (projectName === "x") return;

    this.classList.add("active");

    const { link, content } = this.state.partials?.[projectName] ||
      this.state.partials.blank;

    this.innerHTML = `
      <a href="${link}" target='_blank'>L I N K</a>
      ${content}
    `;

    if (projectName === "zya") {
      document.querySelector("pre#zya-code").style.overflowY = 'scroll'

      hljs.highlightBlock(document.querySelector("pre code"));
    }
  },
});

Zya(".cube", {
  projectSelect({ projectName }) {
    if (projectName === undefined) return;

    if (projectName === "x") return;

    this.style.setProperty("transform", "translate(100vh, 100vh)");
  },

  projectClosed() {
    this.style.setProperty("transform", "unset");
  },

  document({ state }) {
    if (state === "ready") {
      window.setTimeout(() => {
        this.style.setProperty("transform", "unset");
      }, 250);
    }
  },
});

Zya(".cube-selection", {
  pointerClicked({ target }) {
    if (!target || !target.className.includes("grid-link")) return;

    target.classList.add("active");

    const gridLinkIndex = Array.from(this.children)
      .indexOf(target);

    this.dispatch({
      projectSelect: {
        pid: gridLinkIndex,
      },
    });
  },

  pointerTargeted({ target }) {
    if (!target.className.includes("grid-link")) return;

    const gridLinkIndex = Array.from(this.children)
      .indexOf(target);

    this.dispatch({
      projectPreview: {
        pid: gridLinkIndex,
      },
    });
  },
});

Zya(".screen", {
  get state() {
    const titlesShuffled = [
      "blog",
      "zya",
      "cv",
      ...Array
        .from({ length: 9 })
        .fill("x"),
    ]
      .slice(0, 9)
      .sort(() => Math.random() - 0.5);

    return {
      titles: titlesShuffled,
    };
  },

  projectSelect({ pid }) {
    if (!isNaN(pid)) {
      return { projectName: this.state.titles[pid] };
    }
  },

  projectPreview({ pid }) {
    this.parentElement.classList.add("active");

    this.textContent = this.state.titles[pid];
  },
});

Zya(".cube-digi", {
  clickCleared({ target }) {
    this.classList.remove("active");
  },
});

Zya(".title-banner", {
  projectClosed() {
    this.classList.remove("opened");

    this.querySelector("loop-write")
      .setAttribute("data-text", `Welcome Again - `);
  },

  pointerClicked() {
    this.dispatch({ projectClosed: {} });
  },

  projectSelect({ projectName }) {
    if (projectName === undefined) return;

    if (projectName === "x") return;

    this.classList.add("opened");

    if (projectName) {
      this.querySelector("loop-write")
        .setAttribute("data-text", `${projectName} - `);
    }
  },
});

const Main = async function* (stream, dispatch) {
  // TODO - JN - Change to readystate and merge into document process
  document.body.onload = (evt) => dispatch({ document: { state: "ready" } });

  for await (const step of stream) {
    yield step;
  }
};

Zya(PointerEvents);

Zya(Main);
