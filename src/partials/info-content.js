const title = title =>
  `
    <loop-write data-word=${title} data-speed=50 id=intro-loop>
    </loop-write>

    <hr>
  `

export const zya =
  `
    ${title( 'zya' )}

    <p>
      Hello world
    </p>
  `

export const intro =
  `
    ${title( 'intro' )}

    <article class=greeting>
      <p>
        Hello, I'm a Front-End Developer <u class=hidden>, I make clicky web things.</u>
      </p>

      <p>
        I like JavaScript <u class=hidden>, JavaScript likes me.</u>
      </p>
    </article>
  `

export const blog =
  `
    ${title( 'blog' )}

    <article>
      <h2>
        Personal blog
      </h2>

      <a href=https://blog.j-n.me.uk>
        blog.j-n.me.uk
      </a>

      <p>
        A personal blog where that explores different aspects of programming. Most posts focus on Front-End topics, but there are also posts that discuss aspects of functional programming, programming paradigms and the social aspects of development.
      </p>
    </article>
  `

export const zeekode =
  `
    ${title( 'zeekode' )}

    <p>
      Welcome to Zeekode
    </p>
  `

export const blank =
  `
    ${title( 'empty' )}

    <p>
      Empty
    </p>
  `
